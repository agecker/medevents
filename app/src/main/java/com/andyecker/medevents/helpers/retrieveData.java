package com.andyecker.medevents.helpers;

import android.os.AsyncTask;

import com.andyecker.medevents.activities.MainActivity;
import com.andyecker.medevents.models.Event;
import com.andyecker.medevents.models.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import javax.net.ssl.HttpsURLConnection;


public class retrieveData extends AsyncTask<Void,Void,Void> {
    String data = "";
    User user;
    Event[] events;

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL("https://gist.githubusercontent.com/gyoda/c81228219d86076e789119bcb1fabaa1/raw/710e0ef075bc96122de18ac2704cf385f680d162/propeller_mobile_assessment_data.json");

            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();

            InputStream inputStream = httpsURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";
            while(line != null) {
                line = bufferedReader.readLine();
                data = data + line;
            }

            JSONObject jsonObject = new JSONObject(data);

            JSONArray jsonArray = jsonObject.getJSONArray("events");


            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            user = gson.fromJson(jsonObject.getJSONObject("user").toString(), User.class);
            events = gson.fromJson(jsonArray.toString(), Event[].class);


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        MainActivity.userName.setText(this.user.getName());
        MainActivity.address1.setText(this.user.getAddress1());
        MainActivity.address2.setText(this.user.getAddress2());
        MainActivity.uid.setText(this.user.getUid());
        MainActivity.sex.setText(this.user.getSex());
        MainActivity.dob.setText(this.user.getDOB());
        MainActivity.disease.setText(this.user.getDisease());

        Collections.sort(Arrays.asList(this.events), new Comparator<Event>() {
            public int compare(Event o1, Event o2) {
                return o2.getDatetime().compareTo(o1.getDatetime());
            }
        });

        if(MainActivity.eventList.isEmpty()) {
            MainActivity.eventList.addAll(Arrays.asList(this.events));
        }


    }
}
