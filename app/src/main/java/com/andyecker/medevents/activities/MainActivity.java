package com.andyecker.medevents.activities;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.andyecker.medevents.R;
import com.andyecker.medevents.adapters.EventAdapter;
import com.andyecker.medevents.models.Event;
import com.andyecker.medevents.helpers.retrieveData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static TextView userName;
    public static TextView address1;
    public static TextView address2;
    public static TextView uid;
    public static TextView sex;
    public static TextView dob;
    public static TextView disease;

    private RecyclerView mList;
    private Context context;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    public static List<Event> eventList;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.andyecker.medevents.R.layout.activity_main);

        userName = (TextView) findViewById(R.id.textViewName);
        address1 = (TextView) findViewById(R.id.textViewAddress1);
        address2 = (TextView) findViewById(R.id.textViewAddress2);
        uid = (TextView) findViewById(R.id.textViewUID);
        sex = (TextView) findViewById(R.id.textViewSex);
        dob = (TextView) findViewById(R.id.textViewDOB);
        disease = (TextView) findViewById(R.id.textViewDisease);

        final Button addEventButton = findViewById(R.id.addEventButton);
        addEventButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EventActivity.class);
                startActivity(intent);
            }
        });

        mList = findViewById(R.id.eventRecycler);

        eventList = new ArrayList<>();
        adapter = new EventAdapter(getApplicationContext(), eventList);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);

        retrieveData process = new retrieveData();
        process.execute();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Collections.sort(MainActivity.eventList, new Comparator<Event>(){
            public int compare(Event obj1, Event obj2) {
                return obj2.getDatetime().compareTo(obj1.getDatetime());
            }
        });

        adapter.notifyDataSetChanged();
    }

    public static class EventFragment extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.activity_event, container, false);
        }
    }
}
