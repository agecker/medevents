package com.andyecker.medevents.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.andyecker.medevents.R;
import com.andyecker.medevents.models.Event;

import java.util.Calendar;
import java.util.Date;

public class EventActivity extends AppCompatActivity {
    Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.medications_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        event = new Event();


        final Button saveEventButton = findViewById(R.id.saveEventButton);
        saveEventButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Date currentTime = Calendar.getInstance().getTime();

                event.setUid("51c685a4fb20655902014ac3");
                event.setMedication(spinner.getSelectedItem().toString());
                event.setId(MainActivity.eventList.size() + 1);
                event.setDatetime(currentTime.toString());
                event.setMedicationtype((event.getMedication() == "proair") ? "rescue" : "controller");

                MainActivity.eventList.add(event);

                finish();
            }
        });
    }
}
