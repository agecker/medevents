package com.andyecker.medevents.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andyecker.medevents.R;
import com.andyecker.medevents.models.Event;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private Context context;
    private List<Event> list;

    public EventAdapter(Context context, List<Event> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.event_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Event event = list.get(position);

        holder.textUID.setText(event.getUid());
        holder.textDatTime.setText(String.valueOf(event.getDatetime()));
        holder.textMedication.setText(String.valueOf(event.getMedication()));
        holder.textMedicationType.setText(String.valueOf(event.getMedicationtype()));
        holder.textId.setText(String.valueOf(event.getId()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textUID, textDatTime, textMedication, textMedicationType, textId;

        public ViewHolder(View itemView) {
            super(itemView);

            textUID = itemView.findViewById(R.id.textViewEventUID);
            textDatTime = itemView.findViewById(R.id.textViewDateTime);
            textMedication = itemView.findViewById(R.id.textViewMedication);
            textMedicationType = itemView.findViewById(R.id.textViewMedicationType);
            textId = itemView.findViewById(R.id.textViewId);
        }
    }

}